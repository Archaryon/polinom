#include <iostream>
#include <math.h>
#include <fstream>
#include <stdlib.h>
/**
Din greseala de cand am inceput sa citesc tema nu am observat faptul ca scrie "lista este reprezentata in ordine crescatoare"
Intuitiv am facut-o in ordine descrescatoare.

Am implementat o metoda Inverse, care inverseaza polinomul dar nu se pot face operatii pe el deoarece
 metodele date prin supraincarcarea operatorilor nu functioneaza pe polinomul inversat

incercand sa inversez polinomul construit iar din nou sa il inversez la afisare nu functioneaza

este mult prea tarziu sa schimb toate metodele ca sa functioneze pe polinoame ordonate crescator

Am incercat sa pun cat de multe conditii exceptionale am putut (afisarea unui polinom care nu exista,
adunarea a doua polinoame inexistente etc.). Este posibil sa imi fi scapat vreo ceva.

Nu am stiut daca, calculul unui polinom intr-o valoare se realizeaza prin supraincarcarea lui "()"
sau cu o metoda asa ca l-am lasat cu o metoda

Deasemenea am implementat o metoda de sortare a polinomului (imi foloseste la inmultire).

**/
using namespace std;


/* Construirea unei clase nod si anuntarea unei clase Polinom*/
class Polinom;
class Node
{
    double coeficient;
    int exponent;
    Node *next;
    friend class Polinom;
    friend ostream &operator<<(ostream &, const Polinom &); /*istream si ostream sunt friend cu Polinom deci nu au acces la Node decat daca le dau si aici*/
    friend istream &operator>>(istream &, Polinom &);
    ~Node() {if ( next != NULL ) /*Nu stiu daca este necesar dar functioneaza corect*/
                delete next;  }
};


class Polinom
{
    Node *head;

  public:
      /*Aici am metode care nu folosesc neaparat in main ci in ajutorul altor functii*/
    void citire_completa();
    Polinom();
    ~Polinom();
    void New_Node(double, int);
    void Afisare();
    int Calculare(int);
    const Polinom operator+(const Polinom &);
    Polinom &operator=(const Polinom &);
    friend ostream &operator<<(ostream &, const Polinom &);
    friend istream &operator>>(istream &, Polinom &);
    const Polinom operator-(const Polinom &);
    Polinom Actualizare();
    void delete_position(int);
    void delete_first();
    int list_dim();//lungimea polinomului
    const Polinom operator*(const Polinom &);
    void Inverse();
    void sorting();
};

void Polinom::sorting() /*un soi de bubble sort adaptat pe liste*/
{
	Node * aux = head;
	int temp_exponent;
	double temp_coeficient;

	int K = 0;
	while (aux)
	{
		aux = aux->next;
		K++;
	}
	aux = head;

	for (int j=0; j<K; j++)
	{
		while (aux->next)
		{
			if (aux->exponent < aux->next->exponent) /*schimband semnul aici imi ordoneaza crescator sau descrescator*/
			{

				temp_exponent = aux->exponent;
				aux->exponent = aux->next->exponent;
				aux->next->exponent = temp_exponent;

				temp_coeficient = aux->coeficient;
				aux->coeficient = aux->next->coeficient;
				aux->next->coeficient = temp_coeficient;
				aux = aux->next;
			}
			else
				aux = aux->next;
		}
		aux = head;
	}
}

void Polinom::Inverse()/*inversarea unei liste*/
{
    if(head == NULL) return;

    Node *prev = NULL, *current = NULL, *next = NULL;
    current = head;
    while(current != NULL){
        next = current->next;
        current->next = prev;
        prev = current;
        current = next;
    }
    head = prev;
}

Polinom::~Polinom(){ /*destructorul polinomului*/
Node *aux=this->head;
while(aux!=NULL)
{
    Node *temp=aux->next;
    delete aux;
    aux=temp;
}
}

Polinom Polinom::Actualizare(){ /*Pentru monoamele de forma X^2 + X^2 aceasta metoda le contracta sub un singur monom 2*X^2*/
    Polinom rez1;
    Polinom c;
    c=*this;
    Node *aux;
    Node *auy;
    int q=0;
    auy=new Node;
    aux=new Node;
    auy=c.head;
    aux=c.head;
    double suma_coeficienti;
    while(aux!=NULL){
            q++;
            int k=0;
            suma_coeficienti=aux->coeficient;
            auy=aux->next;
        while(auy!=NULL)
        {   k++;
             if(aux->exponent==auy->exponent)
            {suma_coeficienti=suma_coeficienti+auy->coeficient;
            c.delete_position(q+k);}
            auy=auy->next;
        }
        rez1.New_Node(suma_coeficienti,aux->exponent);
        aux=aux->next;
    }

    return rez1;

}

const Polinom Polinom::operator*(const Polinom &b){
    Polinom c,d,rez;
    int branch=0;
    c = b;
    d = *this;
    Node *aux;
    aux = new Node;
    Node *auy;
    auy = new Node;
    if(d.list_dim()>=c.list_dim()){
            branch=1;
            auy = c.head;
            aux = d.head;}
    else {
        branch=2;
        aux=c.head;
        auy=d.head;}
    while(aux!=NULL){
        while(auy!=NULL){
            rez.New_Node(aux->coeficient*auy->coeficient,aux->exponent+auy->exponent);
            auy=auy->next;
        }
            aux=aux->next;
            if(branch==1)
                auy=c.head;
            else if(branch==2)
                auy=d.head;
    }
    Polinom rez2;
    rez2= rez.Actualizare(); /*in urma inmultirii polinomul are monoame de grade egale si nu este nici ordonat deci se apeleaza actualizare si sortare*/
    rez2.sorting();
    return rez2;



}

const Polinom Polinom::operator-(const Polinom &b)/*aceasta supraincarcare a lui - face operatia intre polinomul*/
                                                /*de lungime maxima si polinomul de lungime minima sau egala
                                                deoarece A-B != B-A am avut 2 cazuri k=0 si k=1, cand nu se schimba ordinea*/
                                                /* respectiv cand se schimba ordinea*/
{
    Polinom c,d,rez;
    int k=0;
    c = b;
    d = *this;
    Node *aux;
    aux = new Node;
    Node *auy;
    auy = new Node;
    if(d.list_dim()>=c.list_dim()){
    auy = c.head;
    aux = d.head;
    k=0;
    }
    else {  k=1;
            aux=c.head;
    auy=d.head;}
    if(k==0){
    while(aux!=NULL && auy!=NULL)
    {

        if(aux->exponent>auy->exponent)
        {
            rez.New_Node(aux->coeficient,aux->exponent);
            aux=aux->next;
        }
        else if(aux->exponent==auy->exponent)
        {
            if(aux->coeficient-auy->coeficient!=0)
                rez.New_Node(aux->coeficient-auy->coeficient,aux->exponent);
            aux=aux->next;
            auy=auy->next;
        }
        else if (aux->exponent<auy->exponent)
        {
            rez.New_Node(-auy->coeficient,auy->exponent);
            auy=auy->next;
        }
    }
    if(aux==NULL)
        while(auy!=NULL)
        {
            rez.New_Node(auy->coeficient,auy->exponent);
            auy=auy->next;
        }
    if(auy==NULL)
        while(aux!=NULL)
        {
            rez.New_Node(aux->coeficient,auy->exponent);
            aux=aux->next;
        }
}
    if(k==1){
    while(aux!=NULL && auy!=NULL)
    {

        if(aux->exponent>auy->exponent)
        {
            rez.New_Node(-aux->coeficient,aux->exponent);
            aux=aux->next;
        }
        else if(aux->exponent==auy->exponent)
        {
            if(aux->coeficient-auy->coeficient!=0)
                rez.New_Node(-aux->coeficient+auy->coeficient,aux->exponent);
            aux=aux->next;
            auy=auy->next;
        }
        else if (aux->exponent<auy->exponent)
        {
            rez.New_Node(+auy->coeficient,auy->exponent);
            auy=auy->next;
        }
    }
    if(aux==NULL)
        while(auy!=NULL)
        {
            rez.New_Node(auy->coeficient,auy->exponent);
            auy=auy->next;
        }
    if(auy==NULL)
        while(aux!=NULL)
        {
            rez.New_Node(aux->coeficient,auy->exponent);
            aux=aux->next;
        }
}
    return rez;
}

istream &operator>>(istream &in, Polinom &p)//supraincarcarea lui >> folosind o metoda New_Node care adauga un Node in Polinom
{
    int numar_noduri;
    cout << "inserati numarul de monoame ";
    in >> numar_noduri;
    int k = 1;
    cout<<"Coeficientul va fi de tip double iar exponentul de tip int\n";
    while (k <= numar_noduri)
    {
        double a;
        int b;
        cout << " coeficientul=? ";
        in >> a;
        cout << " exponentul=? ";
        in >> b;
        p.New_Node(a, b);
        k++;
    }
    return in;
}

ostream &operator<<(ostream &out,const Polinom &p) //afisarea polinomului cu exceptii pentru semnul -
{
    Node *aux;
    aux = new Node;
    aux = p.head;
    //out << aux->coeficient << aux->exponent<<" ";
    out<<aux->coeficient << "*X^" << aux->exponent;
    aux=aux->next;
    while (aux != NULL)
    {
        if(aux->coeficient>=0)
        out<<" + "<< aux->coeficient << "*X^" << aux->exponent;
        else out<< aux->coeficient << "*X^" << aux->exponent;
        aux = aux->next;
    }
    return out;
}

Polinom &Polinom::operator=(const Polinom &x) //se parcurge polinomul x si pentru fiecare element se face o copie in this.
{
    if (this != &x)
    {
        Node *aux;
        aux= new Node;
        aux = x.head;
        while (aux != NULL)
        {
            this->New_Node(aux->coeficient, aux->exponent);
            aux = aux->next;
        }
    }

    return *this;
}

int Polinom::list_dim() //dimensiunea Polinomului
{
        Node *aux;
        aux = head;
        int K=0;
       // cout << aux->coeficient << aux->exponent<<" ";
        while (aux != NULL)
        {
           K++;
           aux=aux->next;
        }

        return K;
    }


 void Polinom::delete_first() //foloseste la actualizare
  {
    Node *temp=new Node;
    temp=head;
    head=head->next;
    delete temp;
  }

void Polinom::delete_position(int pos) //foloseste la actualizare
{
    Node *curent=new Node;
    Node *prev=new Node;
    curent=head;
    if(pos>this->list_dim())
        return;
    for(int i=1; i<pos; i++)
    {
        prev=curent;
        curent=curent->next;
    }
    prev->next=curent->next;
}

const Polinom Polinom::operator+(const Polinom &b)
{
    Polinom c,d,rez;
    c = b;
    d = *this;
    Node *aux;
    aux = new Node;
    Node *auy;
    auy = new Node;
    if(d.list_dim()>=c.list_dim()){ //la fel ca la -, se adauca cel mai mic polinom la cel mai mare (ca dimensiune)
    auy = c.head;
    aux = d.head;}
    else {aux=c.head;
    auy=d.head;}
    while(aux!=NULL && auy!=NULL)
    {

        if(aux->exponent>auy->exponent)
        {
            rez.New_Node(aux->coeficient,aux->exponent);
            aux=aux->next;
        }
        else if(aux->exponent==auy->exponent)
        {
            if(aux->coeficient+auy->coeficient!=0)
                rez.New_Node(aux->coeficient+auy->coeficient,aux->exponent);
            aux=aux->next;
            auy=auy->next;
        }
        else if (aux->exponent<auy->exponent)
        {
            rez.New_Node(auy->coeficient,auy->exponent);
            auy=auy->next;
        }
    }
    if(aux==NULL)
        while(auy!=NULL)
        {
            rez.New_Node(auy->coeficient,auy->exponent);
            auy=auy->next;
        }
    if(auy==NULL)
        while(aux!=NULL)
        {
            rez.New_Node(aux->coeficient,auy->exponent);
            aux=aux->next;
        }
    return rez;
}



Polinom::Polinom()
{
    head = NULL;
}

void Polinom::New_Node(double a, int b) //metoda pentru a adauga un nod in polinomul this
{
    Node *aux;
    aux = new Node;
    aux->coeficient = a;
    //   cout << aux->coeficient;
    aux->exponent = b;
    //  cout << aux->exponent;
    Node *last = head;
    aux->next = NULL;
    if (head == NULL)
    {
        //   cout << aux->next;
        head = aux;
        // cout << head->coeficient << head->exponent;
    }
    else
    {
        while (last->next != NULL)
            last = last->next;
        last->next = aux;
        //cout << head->coeficient << head->exponent ;
        //cout<<head->next->coeficient<<head->next->exponent;
    }
   // delete aux;
}
 void Polinom::Afisare() //nu mai este necesara dar o foloseam la inceput, face acelasi lucru ca si ostream
    {
        Node *aux;
        aux = head;
       // cout << aux->coeficient << aux->exponent<<" ";
        while (aux != NULL)
        {
            if(aux->next==NULL)
            cout << aux->coeficient <<"*X^"<< aux->exponent << " ";
            else cout << aux->coeficient <<"*X^"<< aux->exponent << "+";
            aux = aux->next;
        }

    }

int Polinom::Calculare(int q) //calculeaza Polinomul in valoarea q prin insumarea elementelor
{
    Node *aux;
    aux = head;
    int sum = 0;
    while (aux != NULL)
    {
        double a = aux->coeficient;
        int b = aux->exponent;
        sum += a * pow(q, b);
        aux = aux->next;
    }
    return sum;
}

void optiuni(){//o mica constrangere a meniului
    cout<<endl;
    cout<<"1.Afisarea Polinomului n (n citit de la tastatura)\n";
    cout<<"2.Suma a doua Polinoame A si B (A si B - Numarul Polinoamelor citite\n";
    cout<<"3.Diferenta a doua Polinoame A si B (A si B - Numarul Polinoamelor citite\n";
    cout<<"4.Produsul a doua Polinoame A si B (A si B - Numarul Polinoamelor citite\n";
  //  cout<<"5.Atribuirea unui Polinom A valorile Polinomului B (A=B)\n";
    cout<<"5.Calculul Polinomului A in valoarea X (A(X)=?)\n";
    cout<<"6.Afisarea Polinoamelor existente\n";
    cout<<"7.Inversati Polinomul A (A citit de la tastatura)\n";
    cout<<"8.Golire ecran\n";
    cout<<"0.Terminare program\n";
}
int main()
{
    //de aici incepe tot meniul cu implementarea metodelor prin Switch si case fiind loop in While
    //deasemenea am destul de multe verificari de exceptii (mesaje de eroare)
    cout<<"Inceput_Program "<<endl;
    bool a=true;
    int n;
    cout<<"Cititi un numar de polinoame >= 2 \n";
    cin>>n;
    while(n<2){
        cout<<"Eroare! Cititi un numar de polinoame >= 2 \n";
        cin>>n;
    }
    Polinom A[n+1];
    cout<<"\nAti creat cu succes un numar de "<<n<<" Polinoame \n";
    cout<<"Initializare citirea celor "<<n<<" Polinoame: \n";
    cout<<"ATENTIE! Polinoamele se citesc in ordine descrescatoare \n";
    for(int i=1;i<=n;i++)
        {   cout<<"Polinomul numarul "<<i<<"\n";
            cin>>A[i];}
        cout<<"Ati finalizat citirea Polinoamelor\n"<<"Polinoamele arata de forma: \n";
        for (int i=1;i<=n;i++){
            cout<<"Polinomul numarul "<<i<<": ";
            cout<<A[i]<<endl;}
    while(a==true){
        optiuni();
        cout<<"\nIntroduceti numarul optiunii dorite: ";
        int x;
        cin>>x;
        while(x!=1 && x!=2 && x!=3 && x!=4 && x!=5 && x!=6 && x!=7 && x!=8 && x!=0)
        {
            cout<<"\nAti introdus o optiune incorecta! Mai incercati odata: ";
            cin>>x;
        }
        switch(x){

        case 1:{
        int q;
        cout<<"Introduceti numarul Polinomului de afisat: ";
        cin>>q;
        while(q>n && q>0)
            {cout<<"Polinomul nu exista! Mai incercati odata ";
            cout<<"Introduceti numarul Polinomului de afisat: ";
            cin>>q;}
       cout<<A[q];
        cout<<endl; };break;

        case 2:{
        cout<<"Introduceti numarul Polinomului A: ";
        int p,q;
        cin>>q;
        while(q>n || q<0)
        {
            cout<<"Polinomul nu exista! Mai incercati odata \n";
            cout<<"Introduceti numarul Polinomului A: \n";
            cin>>q;
        }
        cout<<"Introduceti numarul Polinomului B: ";
        cin>>p;
        while(p>n || p<0)
        {
            cout<<"Polinomul nu exista! Mai incercati odata \n";
            cout<<"Introduceti numarul Polinomului B: \n";
            cin>>p;
        }
        cout<<A[q]+A[p];
       cout<<endl;  };break;

        case 3:{
        cout<<"Introduceti numarul Polinomului A: ";
        int p,q;
        cin>>q;
        while(q>n || q<0)
        {
            cout<<"Polinomul nu exista! Mai incercati odata \n";
            cout<<"Introduceti numarul Polinomului A: \n";
            cin>>q;
        }
        cout<<"Introduceti numarul Polinomului B: ";
        cin>>p;
       while(p>n || p<0)
        {
            cout<<"Polinomul nu exista! Mai incercati odata \n";
            cout<<"Introduceti numarul Polinomului B: \n";
            cin>>p;
        }
        cout<<A[q]-A[p];
       cout<<endl;  };break;

        case 4:{
        cout<<"Introduceti numarul Polinomului A: ";
        int p,q;
        cin>>q;
        while(q>n || q<0)
        {
            cout<<"Polinomul nu exista! Mai incercati odata \n";
            cout<<"Introduceti numarul Polinomului A: \n";
            cin>>q;
        }
        cout<<"Introduceti numarul Polinomului B: ";
        cin>>p;
        while(p>n || p<0)
        {
            cout<<"Polinomul nu exista! Mai incercati odata \n";
            cout<<"Introduceti numarul Polinomului B: \n";
            cin>>p;
        }
        cout<<A[q]*A[p];
       cout<<endl;  };break;

    /*    case 5:{
        cout<<"Introduceti numarul Polinomului A: ";
        int p,q;
        cin>>q;
        while(q>n || q<0)
        {
            cout<<"Polinomul nu exista! Mai incercati odata \n";
            cout<<"Introduceti numarul Polinomului A: \n";
            cin>>q;
        }
        cout<<"Introduceti numarul Polinomului B: ";
        cin>>p;
        while(p>n || p<0)
        {
            cout<<"Polinomul nu exista! Mai incercati odata \n";
            cout<<"Introduceti numarul Polinomului B: \n";
            cin>>p;
        }


        A[q]=A[p];
        cout<<"Polinomul A arata acum de forma: \n"<<A[q];
        cout<<endl; };break;*/

      case 5:{
        cout<<"Introduceti numarul Polinomului A: ";
        int p,q;
        cin>>q;
        while(q>n || q<0)
        {
            cout<<"Polinomul nu exista! Mai incercati odata \n";
            cout<<"Introduceti numarul Polinomului A: \n";
            cin>>q;
        }
        cout<<"Introduceti valoarea de calcul al Polinomului A: ";
        cin>>p;
        cout<<"Valoarea Polinomului A["<<q<<"] calculata in "<<p<<" este: "<<A[q].Calculare(p);
        cout<<endl;};break;

        case 6:{
        for(int i=1;i<=n;i++)cout<<i<<": "<<A[i]<<endl;};break;

        case 7:{
            cout<<"Inserati Polinomul care urmeaza a fi inversat: ";
            int q;
            cin>>q;
        while(q>n || q<0){
            cout<<"Polinomul nu exista! Mai incercati odata \n";
            cout<<"Introduceti numarul Polinomului A: \n";
            cin>>q;
        }
        A[q].Inverse();
        cout<<A[q];
        };break;

        case 8:{
        system("cls");

        };break;

        case 0:{

        a=false;

        };break;
    }}
    return 0;
}
